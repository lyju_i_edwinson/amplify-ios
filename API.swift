//  This file was automatically generated and should not be edited.

import AWSAppSync

public struct CreateQuizQuestionInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil, question: String) {
    graphQLMap = ["id": id, "question": question]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var question: String {
    get {
      return graphQLMap["question"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "question")
    }
  }
}

public struct UpdateQuizQuestionInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID, question: String? = nil) {
    graphQLMap = ["id": id, "question": question]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var question: String? {
    get {
      return graphQLMap["question"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "question")
    }
  }
}

public struct DeleteQuizQuestionInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct CreateQuizAnswerInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil, answer: String? = nil, `right`: Bool? = nil, question: GraphQLID) {
    graphQLMap = ["id": id, "answer": answer, "right": `right`, "question": question]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var answer: String? {
    get {
      return graphQLMap["answer"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "answer")
    }
  }

  public var `right`: Bool? {
    get {
      return graphQLMap["right"] as! Bool?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "right")
    }
  }

  public var question: GraphQLID {
    get {
      return graphQLMap["question"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "question")
    }
  }
}

public struct UpdateQuizAnswerInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID, answer: String? = nil, `right`: Bool? = nil, question: GraphQLID? = nil) {
    graphQLMap = ["id": id, "answer": answer, "right": `right`, "question": question]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var answer: String? {
    get {
      return graphQLMap["answer"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "answer")
    }
  }

  public var `right`: Bool? {
    get {
      return graphQLMap["right"] as! Bool?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "right")
    }
  }

  public var question: GraphQLID? {
    get {
      return graphQLMap["question"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "question")
    }
  }
}

public struct DeleteQuizAnswerInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct CreateTodelete1Input: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(userId: String, dept: String) {
    graphQLMap = ["userId": userId, "dept": dept]
  }

  public var userId: String {
    get {
      return graphQLMap["userId"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userId")
    }
  }

  public var dept: String {
    get {
      return graphQLMap["dept"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "dept")
    }
  }
}

public struct UpdateTodelete1Input: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(userId: String? = nil, dept: String? = nil) {
    graphQLMap = ["userId": userId, "dept": dept]
  }

  public var userId: String? {
    get {
      return graphQLMap["userId"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userId")
    }
  }

  public var dept: String? {
    get {
      return graphQLMap["dept"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "dept")
    }
  }
}

public struct DeleteTodelete1Input: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct CreateThisIsExistingTableInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(userId: String, itemId: String) {
    graphQLMap = ["userId": userId, "itemId": itemId]
  }

  public var userId: String {
    get {
      return graphQLMap["userId"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userId")
    }
  }

  public var itemId: String {
    get {
      return graphQLMap["itemId"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "itemId")
    }
  }
}

public struct UpdateThisIsExistingTableInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(userId: String? = nil, itemId: String? = nil) {
    graphQLMap = ["userId": userId, "itemId": itemId]
  }

  public var userId: String? {
    get {
      return graphQLMap["userId"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userId")
    }
  }

  public var itemId: String? {
    get {
      return graphQLMap["itemId"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "itemId")
    }
  }
}

public struct DeleteThisIsExistingTableInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct ModelQuizQuestionFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: ModelIDFilterInput? = nil, question: ModelStringFilterInput? = nil, and: [ModelQuizQuestionFilterInput?]? = nil, or: [ModelQuizQuestionFilterInput?]? = nil, not: ModelQuizQuestionFilterInput? = nil) {
    graphQLMap = ["id": id, "question": question, "and": and, "or": or, "not": not]
  }

  public var id: ModelIDFilterInput? {
    get {
      return graphQLMap["id"] as! ModelIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var question: ModelStringFilterInput? {
    get {
      return graphQLMap["question"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "question")
    }
  }

  public var and: [ModelQuizQuestionFilterInput?]? {
    get {
      return graphQLMap["and"] as! [ModelQuizQuestionFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "and")
    }
  }

  public var or: [ModelQuizQuestionFilterInput?]? {
    get {
      return graphQLMap["or"] as! [ModelQuizQuestionFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "or")
    }
  }

  public var not: ModelQuizQuestionFilterInput? {
    get {
      return graphQLMap["not"] as! ModelQuizQuestionFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "not")
    }
  }
}

public struct ModelIDFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: GraphQLID? = nil, eq: GraphQLID? = nil, le: GraphQLID? = nil, lt: GraphQLID? = nil, ge: GraphQLID? = nil, gt: GraphQLID? = nil, contains: GraphQLID? = nil, notContains: GraphQLID? = nil, between: [GraphQLID?]? = nil, beginsWith: GraphQLID? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: GraphQLID? {
    get {
      return graphQLMap["ne"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: GraphQLID? {
    get {
      return graphQLMap["eq"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: GraphQLID? {
    get {
      return graphQLMap["le"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: GraphQLID? {
    get {
      return graphQLMap["lt"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: GraphQLID? {
    get {
      return graphQLMap["ge"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: GraphQLID? {
    get {
      return graphQLMap["gt"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: GraphQLID? {
    get {
      return graphQLMap["contains"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: GraphQLID? {
    get {
      return graphQLMap["notContains"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [GraphQLID?]? {
    get {
      return graphQLMap["between"] as! [GraphQLID?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: GraphQLID? {
    get {
      return graphQLMap["beginsWith"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public struct ModelStringFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: String? = nil, eq: String? = nil, le: String? = nil, lt: String? = nil, ge: String? = nil, gt: String? = nil, contains: String? = nil, notContains: String? = nil, between: [String?]? = nil, beginsWith: String? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: String? {
    get {
      return graphQLMap["ne"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: String? {
    get {
      return graphQLMap["eq"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: String? {
    get {
      return graphQLMap["le"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: String? {
    get {
      return graphQLMap["lt"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: String? {
    get {
      return graphQLMap["ge"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: String? {
    get {
      return graphQLMap["gt"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: String? {
    get {
      return graphQLMap["contains"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: String? {
    get {
      return graphQLMap["notContains"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [String?]? {
    get {
      return graphQLMap["between"] as! [String?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: String? {
    get {
      return graphQLMap["beginsWith"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public struct ModelQuizAnswerFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: ModelIDFilterInput? = nil, answer: ModelStringFilterInput? = nil, `right`: ModelBooleanFilterInput? = nil, question: ModelIDFilterInput? = nil, and: [ModelQuizAnswerFilterInput?]? = nil, or: [ModelQuizAnswerFilterInput?]? = nil, not: ModelQuizAnswerFilterInput? = nil) {
    graphQLMap = ["id": id, "answer": answer, "right": `right`, "question": question, "and": and, "or": or, "not": not]
  }

  public var id: ModelIDFilterInput? {
    get {
      return graphQLMap["id"] as! ModelIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var answer: ModelStringFilterInput? {
    get {
      return graphQLMap["answer"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "answer")
    }
  }

  public var `right`: ModelBooleanFilterInput? {
    get {
      return graphQLMap["right"] as! ModelBooleanFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "right")
    }
  }

  public var question: ModelIDFilterInput? {
    get {
      return graphQLMap["question"] as! ModelIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "question")
    }
  }

  public var and: [ModelQuizAnswerFilterInput?]? {
    get {
      return graphQLMap["and"] as! [ModelQuizAnswerFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "and")
    }
  }

  public var or: [ModelQuizAnswerFilterInput?]? {
    get {
      return graphQLMap["or"] as! [ModelQuizAnswerFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "or")
    }
  }

  public var not: ModelQuizAnswerFilterInput? {
    get {
      return graphQLMap["not"] as! ModelQuizAnswerFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "not")
    }
  }
}

public struct ModelBooleanFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: Bool? = nil, eq: Bool? = nil) {
    graphQLMap = ["ne": ne, "eq": eq]
  }

  public var ne: Bool? {
    get {
      return graphQLMap["ne"] as! Bool?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: Bool? {
    get {
      return graphQLMap["eq"] as! Bool?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }
}

public struct Modeltodelete1FilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(userId: ModelStringFilterInput? = nil, dept: ModelStringFilterInput? = nil, and: [Modeltodelete1FilterInput?]? = nil, or: [Modeltodelete1FilterInput?]? = nil, not: Modeltodelete1FilterInput? = nil) {
    graphQLMap = ["userId": userId, "dept": dept, "and": and, "or": or, "not": not]
  }

  public var userId: ModelStringFilterInput? {
    get {
      return graphQLMap["userId"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userId")
    }
  }

  public var dept: ModelStringFilterInput? {
    get {
      return graphQLMap["dept"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "dept")
    }
  }

  public var and: [Modeltodelete1FilterInput?]? {
    get {
      return graphQLMap["and"] as! [Modeltodelete1FilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "and")
    }
  }

  public var or: [Modeltodelete1FilterInput?]? {
    get {
      return graphQLMap["or"] as! [Modeltodelete1FilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "or")
    }
  }

  public var not: Modeltodelete1FilterInput? {
    get {
      return graphQLMap["not"] as! Modeltodelete1FilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "not")
    }
  }
}

public struct ModelThisIsExistingTableFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(userId: ModelStringFilterInput? = nil, itemId: ModelStringFilterInput? = nil, and: [ModelThisIsExistingTableFilterInput?]? = nil, or: [ModelThisIsExistingTableFilterInput?]? = nil, not: ModelThisIsExistingTableFilterInput? = nil) {
    graphQLMap = ["userId": userId, "itemId": itemId, "and": and, "or": or, "not": not]
  }

  public var userId: ModelStringFilterInput? {
    get {
      return graphQLMap["userId"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userId")
    }
  }

  public var itemId: ModelStringFilterInput? {
    get {
      return graphQLMap["itemId"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "itemId")
    }
  }

  public var and: [ModelThisIsExistingTableFilterInput?]? {
    get {
      return graphQLMap["and"] as! [ModelThisIsExistingTableFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "and")
    }
  }

  public var or: [ModelThisIsExistingTableFilterInput?]? {
    get {
      return graphQLMap["or"] as! [ModelThisIsExistingTableFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "or")
    }
  }

  public var not: ModelThisIsExistingTableFilterInput? {
    get {
      return graphQLMap["not"] as! ModelThisIsExistingTableFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "not")
    }
  }
}

public final class CreateQuizQuestionMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateQuizQuestion($input: CreateQuizQuestionInput!) {\n  createQuizQuestion(input: $input) {\n    __typename\n    id\n    question\n  }\n}"

  public var input: CreateQuizQuestionInput

  public init(input: CreateQuizQuestionInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createQuizQuestion", arguments: ["input": GraphQLVariable("input")], type: .object(CreateQuizQuestion.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createQuizQuestion: CreateQuizQuestion? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createQuizQuestion": createQuizQuestion.flatMap { $0.snapshot }])
    }

    public var createQuizQuestion: CreateQuizQuestion? {
      get {
        return (snapshot["createQuizQuestion"] as? Snapshot).flatMap { CreateQuizQuestion(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createQuizQuestion")
      }
    }

    public struct CreateQuizQuestion: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizQuestion"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("question", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, question: String) {
        self.init(snapshot: ["__typename": "QuizQuestion", "id": id, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var question: String {
        get {
          return snapshot["question"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class UpdateQuizQuestionMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateQuizQuestion($input: UpdateQuizQuestionInput!) {\n  updateQuizQuestion(input: $input) {\n    __typename\n    id\n    question\n  }\n}"

  public var input: UpdateQuizQuestionInput

  public init(input: UpdateQuizQuestionInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateQuizQuestion", arguments: ["input": GraphQLVariable("input")], type: .object(UpdateQuizQuestion.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateQuizQuestion: UpdateQuizQuestion? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateQuizQuestion": updateQuizQuestion.flatMap { $0.snapshot }])
    }

    public var updateQuizQuestion: UpdateQuizQuestion? {
      get {
        return (snapshot["updateQuizQuestion"] as? Snapshot).flatMap { UpdateQuizQuestion(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateQuizQuestion")
      }
    }

    public struct UpdateQuizQuestion: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizQuestion"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("question", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, question: String) {
        self.init(snapshot: ["__typename": "QuizQuestion", "id": id, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var question: String {
        get {
          return snapshot["question"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class DeleteQuizQuestionMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteQuizQuestion($input: DeleteQuizQuestionInput!) {\n  deleteQuizQuestion(input: $input) {\n    __typename\n    id\n    question\n  }\n}"

  public var input: DeleteQuizQuestionInput

  public init(input: DeleteQuizQuestionInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteQuizQuestion", arguments: ["input": GraphQLVariable("input")], type: .object(DeleteQuizQuestion.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteQuizQuestion: DeleteQuizQuestion? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteQuizQuestion": deleteQuizQuestion.flatMap { $0.snapshot }])
    }

    public var deleteQuizQuestion: DeleteQuizQuestion? {
      get {
        return (snapshot["deleteQuizQuestion"] as? Snapshot).flatMap { DeleteQuizQuestion(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteQuizQuestion")
      }
    }

    public struct DeleteQuizQuestion: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizQuestion"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("question", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, question: String) {
        self.init(snapshot: ["__typename": "QuizQuestion", "id": id, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var question: String {
        get {
          return snapshot["question"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class CreateQuizAnswerMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateQuizAnswer($input: CreateQuizAnswerInput!) {\n  createQuizAnswer(input: $input) {\n    __typename\n    id\n    answer\n    right\n    question\n  }\n}"

  public var input: CreateQuizAnswerInput

  public init(input: CreateQuizAnswerInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createQuizAnswer", arguments: ["input": GraphQLVariable("input")], type: .object(CreateQuizAnswer.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createQuizAnswer: CreateQuizAnswer? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createQuizAnswer": createQuizAnswer.flatMap { $0.snapshot }])
    }

    public var createQuizAnswer: CreateQuizAnswer? {
      get {
        return (snapshot["createQuizAnswer"] as? Snapshot).flatMap { CreateQuizAnswer(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createQuizAnswer")
      }
    }

    public struct CreateQuizAnswer: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizAnswer"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("answer", type: .scalar(String.self)),
        GraphQLField("right", type: .scalar(Bool.self)),
        GraphQLField("question", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, answer: String? = nil, `right`: Bool? = nil, question: GraphQLID) {
        self.init(snapshot: ["__typename": "QuizAnswer", "id": id, "answer": answer, "right": `right`, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var answer: String? {
        get {
          return snapshot["answer"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "answer")
        }
      }

      public var `right`: Bool? {
        get {
          return snapshot["right"] as? Bool
        }
        set {
          snapshot.updateValue(newValue, forKey: "right")
        }
      }

      public var question: GraphQLID {
        get {
          return snapshot["question"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class UpdateQuizAnswerMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateQuizAnswer($input: UpdateQuizAnswerInput!) {\n  updateQuizAnswer(input: $input) {\n    __typename\n    id\n    answer\n    right\n    question\n  }\n}"

  public var input: UpdateQuizAnswerInput

  public init(input: UpdateQuizAnswerInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateQuizAnswer", arguments: ["input": GraphQLVariable("input")], type: .object(UpdateQuizAnswer.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateQuizAnswer: UpdateQuizAnswer? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateQuizAnswer": updateQuizAnswer.flatMap { $0.snapshot }])
    }

    public var updateQuizAnswer: UpdateQuizAnswer? {
      get {
        return (snapshot["updateQuizAnswer"] as? Snapshot).flatMap { UpdateQuizAnswer(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateQuizAnswer")
      }
    }

    public struct UpdateQuizAnswer: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizAnswer"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("answer", type: .scalar(String.self)),
        GraphQLField("right", type: .scalar(Bool.self)),
        GraphQLField("question", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, answer: String? = nil, `right`: Bool? = nil, question: GraphQLID) {
        self.init(snapshot: ["__typename": "QuizAnswer", "id": id, "answer": answer, "right": `right`, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var answer: String? {
        get {
          return snapshot["answer"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "answer")
        }
      }

      public var `right`: Bool? {
        get {
          return snapshot["right"] as? Bool
        }
        set {
          snapshot.updateValue(newValue, forKey: "right")
        }
      }

      public var question: GraphQLID {
        get {
          return snapshot["question"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class DeleteQuizAnswerMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteQuizAnswer($input: DeleteQuizAnswerInput!) {\n  deleteQuizAnswer(input: $input) {\n    __typename\n    id\n    answer\n    right\n    question\n  }\n}"

  public var input: DeleteQuizAnswerInput

  public init(input: DeleteQuizAnswerInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteQuizAnswer", arguments: ["input": GraphQLVariable("input")], type: .object(DeleteQuizAnswer.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteQuizAnswer: DeleteQuizAnswer? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteQuizAnswer": deleteQuizAnswer.flatMap { $0.snapshot }])
    }

    public var deleteQuizAnswer: DeleteQuizAnswer? {
      get {
        return (snapshot["deleteQuizAnswer"] as? Snapshot).flatMap { DeleteQuizAnswer(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteQuizAnswer")
      }
    }

    public struct DeleteQuizAnswer: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizAnswer"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("answer", type: .scalar(String.self)),
        GraphQLField("right", type: .scalar(Bool.self)),
        GraphQLField("question", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, answer: String? = nil, `right`: Bool? = nil, question: GraphQLID) {
        self.init(snapshot: ["__typename": "QuizAnswer", "id": id, "answer": answer, "right": `right`, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var answer: String? {
        get {
          return snapshot["answer"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "answer")
        }
      }

      public var `right`: Bool? {
        get {
          return snapshot["right"] as? Bool
        }
        set {
          snapshot.updateValue(newValue, forKey: "right")
        }
      }

      public var question: GraphQLID {
        get {
          return snapshot["question"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class CreateTodelete1Mutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateTodelete1($input: CreateTodelete1Input!) {\n  createTodelete1(input: $input) {\n    __typename\n    userId\n    dept\n  }\n}"

  public var input: CreateTodelete1Input

  public init(input: CreateTodelete1Input) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createTodelete1", arguments: ["input": GraphQLVariable("input")], type: .object(CreateTodelete1.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createTodelete1: CreateTodelete1? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createTodelete1": createTodelete1.flatMap { $0.snapshot }])
    }

    public var createTodelete1: CreateTodelete1? {
      get {
        return (snapshot["createTodelete1"] as? Snapshot).flatMap { CreateTodelete1(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createTodelete1")
      }
    }

    public struct CreateTodelete1: GraphQLSelectionSet {
      public static let possibleTypes = ["todelete1"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("dept", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, dept: String) {
        self.init(snapshot: ["__typename": "todelete1", "userId": userId, "dept": dept])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var dept: String {
        get {
          return snapshot["dept"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "dept")
        }
      }
    }
  }
}

public final class UpdateTodelete1Mutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateTodelete1($input: UpdateTodelete1Input!) {\n  updateTodelete1(input: $input) {\n    __typename\n    userId\n    dept\n  }\n}"

  public var input: UpdateTodelete1Input

  public init(input: UpdateTodelete1Input) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateTodelete1", arguments: ["input": GraphQLVariable("input")], type: .object(UpdateTodelete1.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateTodelete1: UpdateTodelete1? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateTodelete1": updateTodelete1.flatMap { $0.snapshot }])
    }

    public var updateTodelete1: UpdateTodelete1? {
      get {
        return (snapshot["updateTodelete1"] as? Snapshot).flatMap { UpdateTodelete1(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateTodelete1")
      }
    }

    public struct UpdateTodelete1: GraphQLSelectionSet {
      public static let possibleTypes = ["todelete1"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("dept", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, dept: String) {
        self.init(snapshot: ["__typename": "todelete1", "userId": userId, "dept": dept])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var dept: String {
        get {
          return snapshot["dept"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "dept")
        }
      }
    }
  }
}

public final class DeleteTodelete1Mutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteTodelete1($input: DeleteTodelete1Input!) {\n  deleteTodelete1(input: $input) {\n    __typename\n    userId\n    dept\n  }\n}"

  public var input: DeleteTodelete1Input

  public init(input: DeleteTodelete1Input) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteTodelete1", arguments: ["input": GraphQLVariable("input")], type: .object(DeleteTodelete1.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteTodelete1: DeleteTodelete1? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteTodelete1": deleteTodelete1.flatMap { $0.snapshot }])
    }

    public var deleteTodelete1: DeleteTodelete1? {
      get {
        return (snapshot["deleteTodelete1"] as? Snapshot).flatMap { DeleteTodelete1(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteTodelete1")
      }
    }

    public struct DeleteTodelete1: GraphQLSelectionSet {
      public static let possibleTypes = ["todelete1"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("dept", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, dept: String) {
        self.init(snapshot: ["__typename": "todelete1", "userId": userId, "dept": dept])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var dept: String {
        get {
          return snapshot["dept"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "dept")
        }
      }
    }
  }
}

public final class CreateThisIsExistingTableMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateThisIsExistingTable($input: CreateThisIsExistingTableInput!) {\n  createThisIsExistingTable(input: $input) {\n    __typename\n    userId\n    itemId\n  }\n}"

  public var input: CreateThisIsExistingTableInput

  public init(input: CreateThisIsExistingTableInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createThisIsExistingTable", arguments: ["input": GraphQLVariable("input")], type: .object(CreateThisIsExistingTable.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createThisIsExistingTable: CreateThisIsExistingTable? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createThisIsExistingTable": createThisIsExistingTable.flatMap { $0.snapshot }])
    }

    public var createThisIsExistingTable: CreateThisIsExistingTable? {
      get {
        return (snapshot["createThisIsExistingTable"] as? Snapshot).flatMap { CreateThisIsExistingTable(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createThisIsExistingTable")
      }
    }

    public struct CreateThisIsExistingTable: GraphQLSelectionSet {
      public static let possibleTypes = ["ThisIsExistingTable"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("itemId", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, itemId: String) {
        self.init(snapshot: ["__typename": "ThisIsExistingTable", "userId": userId, "itemId": itemId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var itemId: String {
        get {
          return snapshot["itemId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "itemId")
        }
      }
    }
  }
}

public final class UpdateThisIsExistingTableMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateThisIsExistingTable($input: UpdateThisIsExistingTableInput!) {\n  updateThisIsExistingTable(input: $input) {\n    __typename\n    userId\n    itemId\n  }\n}"

  public var input: UpdateThisIsExistingTableInput

  public init(input: UpdateThisIsExistingTableInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateThisIsExistingTable", arguments: ["input": GraphQLVariable("input")], type: .object(UpdateThisIsExistingTable.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateThisIsExistingTable: UpdateThisIsExistingTable? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateThisIsExistingTable": updateThisIsExistingTable.flatMap { $0.snapshot }])
    }

    public var updateThisIsExistingTable: UpdateThisIsExistingTable? {
      get {
        return (snapshot["updateThisIsExistingTable"] as? Snapshot).flatMap { UpdateThisIsExistingTable(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateThisIsExistingTable")
      }
    }

    public struct UpdateThisIsExistingTable: GraphQLSelectionSet {
      public static let possibleTypes = ["ThisIsExistingTable"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("itemId", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, itemId: String) {
        self.init(snapshot: ["__typename": "ThisIsExistingTable", "userId": userId, "itemId": itemId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var itemId: String {
        get {
          return snapshot["itemId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "itemId")
        }
      }
    }
  }
}

public final class DeleteThisIsExistingTableMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteThisIsExistingTable($input: DeleteThisIsExistingTableInput!) {\n  deleteThisIsExistingTable(input: $input) {\n    __typename\n    userId\n    itemId\n  }\n}"

  public var input: DeleteThisIsExistingTableInput

  public init(input: DeleteThisIsExistingTableInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteThisIsExistingTable", arguments: ["input": GraphQLVariable("input")], type: .object(DeleteThisIsExistingTable.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteThisIsExistingTable: DeleteThisIsExistingTable? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteThisIsExistingTable": deleteThisIsExistingTable.flatMap { $0.snapshot }])
    }

    public var deleteThisIsExistingTable: DeleteThisIsExistingTable? {
      get {
        return (snapshot["deleteThisIsExistingTable"] as? Snapshot).flatMap { DeleteThisIsExistingTable(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteThisIsExistingTable")
      }
    }

    public struct DeleteThisIsExistingTable: GraphQLSelectionSet {
      public static let possibleTypes = ["ThisIsExistingTable"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("itemId", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, itemId: String) {
        self.init(snapshot: ["__typename": "ThisIsExistingTable", "userId": userId, "itemId": itemId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var itemId: String {
        get {
          return snapshot["itemId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "itemId")
        }
      }
    }
  }
}

public final class GetQuizQuestionQuery: GraphQLQuery {
  public static let operationString =
    "query GetQuizQuestion($id: ID!) {\n  getQuizQuestion(id: $id) {\n    __typename\n    id\n    question\n  }\n}"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getQuizQuestion", arguments: ["id": GraphQLVariable("id")], type: .object(GetQuizQuestion.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getQuizQuestion: GetQuizQuestion? = nil) {
      self.init(snapshot: ["__typename": "Query", "getQuizQuestion": getQuizQuestion.flatMap { $0.snapshot }])
    }

    public var getQuizQuestion: GetQuizQuestion? {
      get {
        return (snapshot["getQuizQuestion"] as? Snapshot).flatMap { GetQuizQuestion(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getQuizQuestion")
      }
    }

    public struct GetQuizQuestion: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizQuestion"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("question", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, question: String) {
        self.init(snapshot: ["__typename": "QuizQuestion", "id": id, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var question: String {
        get {
          return snapshot["question"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class ListQuizQuestionsQuery: GraphQLQuery {
  public static let operationString =
    "query ListQuizQuestions($filter: ModelQuizQuestionFilterInput, $limit: Int, $nextToken: String) {\n  listQuizQuestions(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      id\n      question\n    }\n    nextToken\n  }\n}"

  public var filter: ModelQuizQuestionFilterInput?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: ModelQuizQuestionFilterInput? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listQuizQuestions", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListQuizQuestion.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listQuizQuestions: ListQuizQuestion? = nil) {
      self.init(snapshot: ["__typename": "Query", "listQuizQuestions": listQuizQuestions.flatMap { $0.snapshot }])
    }

    public var listQuizQuestions: ListQuizQuestion? {
      get {
        return (snapshot["listQuizQuestions"] as? Snapshot).flatMap { ListQuizQuestion(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listQuizQuestions")
      }
    }

    public struct ListQuizQuestion: GraphQLSelectionSet {
      public static let possibleTypes = ["ModelQuizQuestionConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "ModelQuizQuestionConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["QuizQuestion"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("question", type: .nonNull(.scalar(String.self))),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, question: String) {
          self.init(snapshot: ["__typename": "QuizQuestion", "id": id, "question": question])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var question: String {
          get {
            return snapshot["question"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "question")
          }
        }
      }
    }
  }
}

public final class GetQuizAnswerQuery: GraphQLQuery {
  public static let operationString =
    "query GetQuizAnswer($id: ID!) {\n  getQuizAnswer(id: $id) {\n    __typename\n    id\n    answer\n    right\n    question\n  }\n}"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getQuizAnswer", arguments: ["id": GraphQLVariable("id")], type: .object(GetQuizAnswer.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getQuizAnswer: GetQuizAnswer? = nil) {
      self.init(snapshot: ["__typename": "Query", "getQuizAnswer": getQuizAnswer.flatMap { $0.snapshot }])
    }

    public var getQuizAnswer: GetQuizAnswer? {
      get {
        return (snapshot["getQuizAnswer"] as? Snapshot).flatMap { GetQuizAnswer(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getQuizAnswer")
      }
    }

    public struct GetQuizAnswer: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizAnswer"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("answer", type: .scalar(String.self)),
        GraphQLField("right", type: .scalar(Bool.self)),
        GraphQLField("question", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, answer: String? = nil, `right`: Bool? = nil, question: GraphQLID) {
        self.init(snapshot: ["__typename": "QuizAnswer", "id": id, "answer": answer, "right": `right`, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var answer: String? {
        get {
          return snapshot["answer"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "answer")
        }
      }

      public var `right`: Bool? {
        get {
          return snapshot["right"] as? Bool
        }
        set {
          snapshot.updateValue(newValue, forKey: "right")
        }
      }

      public var question: GraphQLID {
        get {
          return snapshot["question"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class ListQuizAnswersQuery: GraphQLQuery {
  public static let operationString =
    "query ListQuizAnswers($filter: ModelQuizAnswerFilterInput, $limit: Int, $nextToken: String) {\n  listQuizAnswers(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      id\n      answer\n      right\n      question\n    }\n    nextToken\n  }\n}"

  public var filter: ModelQuizAnswerFilterInput?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: ModelQuizAnswerFilterInput? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listQuizAnswers", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListQuizAnswer.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listQuizAnswers: ListQuizAnswer? = nil) {
      self.init(snapshot: ["__typename": "Query", "listQuizAnswers": listQuizAnswers.flatMap { $0.snapshot }])
    }

    public var listQuizAnswers: ListQuizAnswer? {
      get {
        return (snapshot["listQuizAnswers"] as? Snapshot).flatMap { ListQuizAnswer(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listQuizAnswers")
      }
    }

    public struct ListQuizAnswer: GraphQLSelectionSet {
      public static let possibleTypes = ["ModelQuizAnswerConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "ModelQuizAnswerConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["QuizAnswer"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("answer", type: .scalar(String.self)),
          GraphQLField("right", type: .scalar(Bool.self)),
          GraphQLField("question", type: .nonNull(.scalar(GraphQLID.self))),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, answer: String? = nil, `right`: Bool? = nil, question: GraphQLID) {
          self.init(snapshot: ["__typename": "QuizAnswer", "id": id, "answer": answer, "right": `right`, "question": question])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var answer: String? {
          get {
            return snapshot["answer"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "answer")
          }
        }

        public var `right`: Bool? {
          get {
            return snapshot["right"] as? Bool
          }
          set {
            snapshot.updateValue(newValue, forKey: "right")
          }
        }

        public var question: GraphQLID {
          get {
            return snapshot["question"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "question")
          }
        }
      }
    }
  }
}

public final class GetTodelete1Query: GraphQLQuery {
  public static let operationString =
    "query GetTodelete1($id: ID!) {\n  getTodelete1(id: $id) {\n    __typename\n    userId\n    dept\n  }\n}"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getTodelete1", arguments: ["id": GraphQLVariable("id")], type: .object(GetTodelete1.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getTodelete1: GetTodelete1? = nil) {
      self.init(snapshot: ["__typename": "Query", "getTodelete1": getTodelete1.flatMap { $0.snapshot }])
    }

    public var getTodelete1: GetTodelete1? {
      get {
        return (snapshot["getTodelete1"] as? Snapshot).flatMap { GetTodelete1(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getTodelete1")
      }
    }

    public struct GetTodelete1: GraphQLSelectionSet {
      public static let possibleTypes = ["todelete1"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("dept", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, dept: String) {
        self.init(snapshot: ["__typename": "todelete1", "userId": userId, "dept": dept])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var dept: String {
        get {
          return snapshot["dept"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "dept")
        }
      }
    }
  }
}

public final class ListTodelete1sQuery: GraphQLQuery {
  public static let operationString =
    "query ListTodelete1s($filter: Modeltodelete1FilterInput, $limit: Int, $nextToken: String) {\n  listTodelete1s(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      userId\n      dept\n    }\n    nextToken\n  }\n}"

  public var filter: Modeltodelete1FilterInput?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: Modeltodelete1FilterInput? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listTodelete1s", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListTodelete1.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listTodelete1s: ListTodelete1? = nil) {
      self.init(snapshot: ["__typename": "Query", "listTodelete1s": listTodelete1s.flatMap { $0.snapshot }])
    }

    public var listTodelete1s: ListTodelete1? {
      get {
        return (snapshot["listTodelete1s"] as? Snapshot).flatMap { ListTodelete1(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listTodelete1s")
      }
    }

    public struct ListTodelete1: GraphQLSelectionSet {
      public static let possibleTypes = ["Modeltodelete1Connection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "Modeltodelete1Connection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["todelete1"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
          GraphQLField("dept", type: .nonNull(.scalar(String.self))),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(userId: String, dept: String) {
          self.init(snapshot: ["__typename": "todelete1", "userId": userId, "dept": dept])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return snapshot["userId"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "userId")
          }
        }

        public var dept: String {
          get {
            return snapshot["dept"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "dept")
          }
        }
      }
    }
  }
}

public final class GetThisIsExistingTableQuery: GraphQLQuery {
  public static let operationString =
    "query GetThisIsExistingTable($id: ID!) {\n  getThisIsExistingTable(id: $id) {\n    __typename\n    userId\n    itemId\n  }\n}"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getThisIsExistingTable", arguments: ["id": GraphQLVariable("id")], type: .object(GetThisIsExistingTable.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getThisIsExistingTable: GetThisIsExistingTable? = nil) {
      self.init(snapshot: ["__typename": "Query", "getThisIsExistingTable": getThisIsExistingTable.flatMap { $0.snapshot }])
    }

    public var getThisIsExistingTable: GetThisIsExistingTable? {
      get {
        return (snapshot["getThisIsExistingTable"] as? Snapshot).flatMap { GetThisIsExistingTable(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getThisIsExistingTable")
      }
    }

    public struct GetThisIsExistingTable: GraphQLSelectionSet {
      public static let possibleTypes = ["ThisIsExistingTable"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("itemId", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, itemId: String) {
        self.init(snapshot: ["__typename": "ThisIsExistingTable", "userId": userId, "itemId": itemId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var itemId: String {
        get {
          return snapshot["itemId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "itemId")
        }
      }
    }
  }
}

public final class ListThisIsExistingTablesQuery: GraphQLQuery {
  public static let operationString =
    "query ListThisIsExistingTables($filter: ModelThisIsExistingTableFilterInput, $limit: Int, $nextToken: String) {\n  listThisIsExistingTables(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      userId\n      itemId\n    }\n    nextToken\n  }\n}"

  public var filter: ModelThisIsExistingTableFilterInput?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: ModelThisIsExistingTableFilterInput? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listThisIsExistingTables", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListThisIsExistingTable.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listThisIsExistingTables: ListThisIsExistingTable? = nil) {
      self.init(snapshot: ["__typename": "Query", "listThisIsExistingTables": listThisIsExistingTables.flatMap { $0.snapshot }])
    }

    public var listThisIsExistingTables: ListThisIsExistingTable? {
      get {
        return (snapshot["listThisIsExistingTables"] as? Snapshot).flatMap { ListThisIsExistingTable(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listThisIsExistingTables")
      }
    }

    public struct ListThisIsExistingTable: GraphQLSelectionSet {
      public static let possibleTypes = ["ModelThisIsExistingTableConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "ModelThisIsExistingTableConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["ThisIsExistingTable"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
          GraphQLField("itemId", type: .nonNull(.scalar(String.self))),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(userId: String, itemId: String) {
          self.init(snapshot: ["__typename": "ThisIsExistingTable", "userId": userId, "itemId": itemId])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return snapshot["userId"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "userId")
          }
        }

        public var itemId: String {
          get {
            return snapshot["itemId"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "itemId")
          }
        }
      }
    }
  }
}

public final class OnCreateQuizQuestionSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateQuizQuestion {\n  onCreateQuizQuestion {\n    __typename\n    id\n    question\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateQuizQuestion", type: .object(OnCreateQuizQuestion.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateQuizQuestion: OnCreateQuizQuestion? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateQuizQuestion": onCreateQuizQuestion.flatMap { $0.snapshot }])
    }

    public var onCreateQuizQuestion: OnCreateQuizQuestion? {
      get {
        return (snapshot["onCreateQuizQuestion"] as? Snapshot).flatMap { OnCreateQuizQuestion(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateQuizQuestion")
      }
    }

    public struct OnCreateQuizQuestion: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizQuestion"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("question", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, question: String) {
        self.init(snapshot: ["__typename": "QuizQuestion", "id": id, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var question: String {
        get {
          return snapshot["question"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class OnUpdateQuizQuestionSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdateQuizQuestion {\n  onUpdateQuizQuestion {\n    __typename\n    id\n    question\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdateQuizQuestion", type: .object(OnUpdateQuizQuestion.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdateQuizQuestion: OnUpdateQuizQuestion? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdateQuizQuestion": onUpdateQuizQuestion.flatMap { $0.snapshot }])
    }

    public var onUpdateQuizQuestion: OnUpdateQuizQuestion? {
      get {
        return (snapshot["onUpdateQuizQuestion"] as? Snapshot).flatMap { OnUpdateQuizQuestion(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdateQuizQuestion")
      }
    }

    public struct OnUpdateQuizQuestion: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizQuestion"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("question", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, question: String) {
        self.init(snapshot: ["__typename": "QuizQuestion", "id": id, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var question: String {
        get {
          return snapshot["question"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class OnDeleteQuizQuestionSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeleteQuizQuestion {\n  onDeleteQuizQuestion {\n    __typename\n    id\n    question\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeleteQuizQuestion", type: .object(OnDeleteQuizQuestion.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeleteQuizQuestion: OnDeleteQuizQuestion? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeleteQuizQuestion": onDeleteQuizQuestion.flatMap { $0.snapshot }])
    }

    public var onDeleteQuizQuestion: OnDeleteQuizQuestion? {
      get {
        return (snapshot["onDeleteQuizQuestion"] as? Snapshot).flatMap { OnDeleteQuizQuestion(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeleteQuizQuestion")
      }
    }

    public struct OnDeleteQuizQuestion: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizQuestion"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("question", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, question: String) {
        self.init(snapshot: ["__typename": "QuizQuestion", "id": id, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var question: String {
        get {
          return snapshot["question"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class OnCreateQuizAnswerSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateQuizAnswer {\n  onCreateQuizAnswer {\n    __typename\n    id\n    answer\n    right\n    question\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateQuizAnswer", type: .object(OnCreateQuizAnswer.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateQuizAnswer: OnCreateQuizAnswer? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateQuizAnswer": onCreateQuizAnswer.flatMap { $0.snapshot }])
    }

    public var onCreateQuizAnswer: OnCreateQuizAnswer? {
      get {
        return (snapshot["onCreateQuizAnswer"] as? Snapshot).flatMap { OnCreateQuizAnswer(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateQuizAnswer")
      }
    }

    public struct OnCreateQuizAnswer: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizAnswer"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("answer", type: .scalar(String.self)),
        GraphQLField("right", type: .scalar(Bool.self)),
        GraphQLField("question", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, answer: String? = nil, `right`: Bool? = nil, question: GraphQLID) {
        self.init(snapshot: ["__typename": "QuizAnswer", "id": id, "answer": answer, "right": `right`, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var answer: String? {
        get {
          return snapshot["answer"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "answer")
        }
      }

      public var `right`: Bool? {
        get {
          return snapshot["right"] as? Bool
        }
        set {
          snapshot.updateValue(newValue, forKey: "right")
        }
      }

      public var question: GraphQLID {
        get {
          return snapshot["question"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class OnUpdateQuizAnswerSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdateQuizAnswer {\n  onUpdateQuizAnswer {\n    __typename\n    id\n    answer\n    right\n    question\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdateQuizAnswer", type: .object(OnUpdateQuizAnswer.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdateQuizAnswer: OnUpdateQuizAnswer? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdateQuizAnswer": onUpdateQuizAnswer.flatMap { $0.snapshot }])
    }

    public var onUpdateQuizAnswer: OnUpdateQuizAnswer? {
      get {
        return (snapshot["onUpdateQuizAnswer"] as? Snapshot).flatMap { OnUpdateQuizAnswer(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdateQuizAnswer")
      }
    }

    public struct OnUpdateQuizAnswer: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizAnswer"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("answer", type: .scalar(String.self)),
        GraphQLField("right", type: .scalar(Bool.self)),
        GraphQLField("question", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, answer: String? = nil, `right`: Bool? = nil, question: GraphQLID) {
        self.init(snapshot: ["__typename": "QuizAnswer", "id": id, "answer": answer, "right": `right`, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var answer: String? {
        get {
          return snapshot["answer"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "answer")
        }
      }

      public var `right`: Bool? {
        get {
          return snapshot["right"] as? Bool
        }
        set {
          snapshot.updateValue(newValue, forKey: "right")
        }
      }

      public var question: GraphQLID {
        get {
          return snapshot["question"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class OnDeleteQuizAnswerSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeleteQuizAnswer {\n  onDeleteQuizAnswer {\n    __typename\n    id\n    answer\n    right\n    question\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeleteQuizAnswer", type: .object(OnDeleteQuizAnswer.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeleteQuizAnswer: OnDeleteQuizAnswer? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeleteQuizAnswer": onDeleteQuizAnswer.flatMap { $0.snapshot }])
    }

    public var onDeleteQuizAnswer: OnDeleteQuizAnswer? {
      get {
        return (snapshot["onDeleteQuizAnswer"] as? Snapshot).flatMap { OnDeleteQuizAnswer(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeleteQuizAnswer")
      }
    }

    public struct OnDeleteQuizAnswer: GraphQLSelectionSet {
      public static let possibleTypes = ["QuizAnswer"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("answer", type: .scalar(String.self)),
        GraphQLField("right", type: .scalar(Bool.self)),
        GraphQLField("question", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, answer: String? = nil, `right`: Bool? = nil, question: GraphQLID) {
        self.init(snapshot: ["__typename": "QuizAnswer", "id": id, "answer": answer, "right": `right`, "question": question])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var answer: String? {
        get {
          return snapshot["answer"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "answer")
        }
      }

      public var `right`: Bool? {
        get {
          return snapshot["right"] as? Bool
        }
        set {
          snapshot.updateValue(newValue, forKey: "right")
        }
      }

      public var question: GraphQLID {
        get {
          return snapshot["question"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "question")
        }
      }
    }
  }
}

public final class OnCreateTodelete1Subscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateTodelete1 {\n  onCreateTodelete1 {\n    __typename\n    userId\n    dept\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateTodelete1", type: .object(OnCreateTodelete1.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateTodelete1: OnCreateTodelete1? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateTodelete1": onCreateTodelete1.flatMap { $0.snapshot }])
    }

    public var onCreateTodelete1: OnCreateTodelete1? {
      get {
        return (snapshot["onCreateTodelete1"] as? Snapshot).flatMap { OnCreateTodelete1(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateTodelete1")
      }
    }

    public struct OnCreateTodelete1: GraphQLSelectionSet {
      public static let possibleTypes = ["todelete1"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("dept", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, dept: String) {
        self.init(snapshot: ["__typename": "todelete1", "userId": userId, "dept": dept])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var dept: String {
        get {
          return snapshot["dept"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "dept")
        }
      }
    }
  }
}

public final class OnUpdateTodelete1Subscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdateTodelete1 {\n  onUpdateTodelete1 {\n    __typename\n    userId\n    dept\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdateTodelete1", type: .object(OnUpdateTodelete1.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdateTodelete1: OnUpdateTodelete1? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdateTodelete1": onUpdateTodelete1.flatMap { $0.snapshot }])
    }

    public var onUpdateTodelete1: OnUpdateTodelete1? {
      get {
        return (snapshot["onUpdateTodelete1"] as? Snapshot).flatMap { OnUpdateTodelete1(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdateTodelete1")
      }
    }

    public struct OnUpdateTodelete1: GraphQLSelectionSet {
      public static let possibleTypes = ["todelete1"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("dept", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, dept: String) {
        self.init(snapshot: ["__typename": "todelete1", "userId": userId, "dept": dept])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var dept: String {
        get {
          return snapshot["dept"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "dept")
        }
      }
    }
  }
}

public final class OnDeleteTodelete1Subscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeleteTodelete1 {\n  onDeleteTodelete1 {\n    __typename\n    userId\n    dept\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeleteTodelete1", type: .object(OnDeleteTodelete1.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeleteTodelete1: OnDeleteTodelete1? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeleteTodelete1": onDeleteTodelete1.flatMap { $0.snapshot }])
    }

    public var onDeleteTodelete1: OnDeleteTodelete1? {
      get {
        return (snapshot["onDeleteTodelete1"] as? Snapshot).flatMap { OnDeleteTodelete1(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeleteTodelete1")
      }
    }

    public struct OnDeleteTodelete1: GraphQLSelectionSet {
      public static let possibleTypes = ["todelete1"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("dept", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, dept: String) {
        self.init(snapshot: ["__typename": "todelete1", "userId": userId, "dept": dept])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var dept: String {
        get {
          return snapshot["dept"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "dept")
        }
      }
    }
  }
}

public final class OnCreateThisIsExistingTableSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateThisIsExistingTable {\n  onCreateThisIsExistingTable {\n    __typename\n    userId\n    itemId\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateThisIsExistingTable", type: .object(OnCreateThisIsExistingTable.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateThisIsExistingTable: OnCreateThisIsExistingTable? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateThisIsExistingTable": onCreateThisIsExistingTable.flatMap { $0.snapshot }])
    }

    public var onCreateThisIsExistingTable: OnCreateThisIsExistingTable? {
      get {
        return (snapshot["onCreateThisIsExistingTable"] as? Snapshot).flatMap { OnCreateThisIsExistingTable(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateThisIsExistingTable")
      }
    }

    public struct OnCreateThisIsExistingTable: GraphQLSelectionSet {
      public static let possibleTypes = ["ThisIsExistingTable"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("itemId", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, itemId: String) {
        self.init(snapshot: ["__typename": "ThisIsExistingTable", "userId": userId, "itemId": itemId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var itemId: String {
        get {
          return snapshot["itemId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "itemId")
        }
      }
    }
  }
}

public final class OnUpdateThisIsExistingTableSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdateThisIsExistingTable {\n  onUpdateThisIsExistingTable {\n    __typename\n    userId\n    itemId\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdateThisIsExistingTable", type: .object(OnUpdateThisIsExistingTable.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdateThisIsExistingTable: OnUpdateThisIsExistingTable? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdateThisIsExistingTable": onUpdateThisIsExistingTable.flatMap { $0.snapshot }])
    }

    public var onUpdateThisIsExistingTable: OnUpdateThisIsExistingTable? {
      get {
        return (snapshot["onUpdateThisIsExistingTable"] as? Snapshot).flatMap { OnUpdateThisIsExistingTable(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdateThisIsExistingTable")
      }
    }

    public struct OnUpdateThisIsExistingTable: GraphQLSelectionSet {
      public static let possibleTypes = ["ThisIsExistingTable"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("itemId", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, itemId: String) {
        self.init(snapshot: ["__typename": "ThisIsExistingTable", "userId": userId, "itemId": itemId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var itemId: String {
        get {
          return snapshot["itemId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "itemId")
        }
      }
    }
  }
}

public final class OnDeleteThisIsExistingTableSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeleteThisIsExistingTable {\n  onDeleteThisIsExistingTable {\n    __typename\n    userId\n    itemId\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeleteThisIsExistingTable", type: .object(OnDeleteThisIsExistingTable.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeleteThisIsExistingTable: OnDeleteThisIsExistingTable? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeleteThisIsExistingTable": onDeleteThisIsExistingTable.flatMap { $0.snapshot }])
    }

    public var onDeleteThisIsExistingTable: OnDeleteThisIsExistingTable? {
      get {
        return (snapshot["onDeleteThisIsExistingTable"] as? Snapshot).flatMap { OnDeleteThisIsExistingTable(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeleteThisIsExistingTable")
      }
    }

    public struct OnDeleteThisIsExistingTable: GraphQLSelectionSet {
      public static let possibleTypes = ["ThisIsExistingTable"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("itemId", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, itemId: String) {
        self.init(snapshot: ["__typename": "ThisIsExistingTable", "userId": userId, "itemId": itemId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var itemId: String {
        get {
          return snapshot["itemId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "itemId")
        }
      }
    }
  }
}