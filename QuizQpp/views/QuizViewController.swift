//
//  QuizViewController.swift
//  QuizQpp
//
//  Created by Lyju Edwinson on 29/3/19.
//  Copyright © 2019 Lyju Edwinson. All rights reserved.
//

import UIKit
import SwiftMessages
import AWSAppSync

class QuizViewController: UIViewController {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel1: UILabel!
    @IBOutlet weak var answerLabel2: UILabel!
    @IBOutlet weak var answerLabel3: UILabel!
    
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    
    
    var questions: [Question] = []
    var answers: [Answer] = []
    let quizService = QuizService()
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setQuestion()
        self.setTapGestures()
        
        self.nextBtn.isEnabled = false
    }
    
    func setQuestion() {
        quizService.fetchQuestions { (questions) in
            self.questions.append(contentsOf: questions)
            self.questionLabel.text = self.questions[self.currentIndex].question
            
            self.setAnswers(questionId: self.questions[self.currentIndex].id!)
            
            self.currentIndex = self.currentIndex + 1
            if (self.currentIndex >= questions.count) {
                self.currentIndex = 0;
            }
        }
    }
    
    func setAnswers(questionId: GraphQLID) {
        quizService.fetchAnswersFor(questionId: questionId, afterFetchingAQnswers: { (answers) in
            self.answers = answers
            self.answerLabel1.text = answers[0].answer
            self.answerLabel2.text = answers[1].answer
            self.answerLabel3.text = answers[2].answer
        })
    }
    
    @objc func answerLabelTapped(sender: UITapGestureRecognizer) {
        let labelTapped = sender.view as! UILabel
        if (self.answers[labelTapped.tag].right) {
            self.showMessage(title: "Congratulations", content: "Well done....", theme: .success)
            self.nextBtn.isEnabled = true
        } else {
            self.showMessage(title: "Error", content: "Wrong answer... try again...", theme: .error)
        }
    }
    
    func showMessage(title: String, content: String, theme: Theme) {
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(theme)
        view.configureContent(title: title, body: content)
        view.button?.isHidden = true
        SwiftMessages.show(view: view)
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        self.dismiss(animated: true) {
            //nothing
        }
    }
    
    func setTapGestures() {
        let tap1 = UITapGestureRecognizer(target: self, action:
            #selector(QuizViewController.answerLabelTapped))
        answerLabel1.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action:
            #selector(QuizViewController.answerLabelTapped))
        answerLabel2.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action:
            #selector(QuizViewController.answerLabelTapped))
        answerLabel3.addGestureRecognizer(tap3)
    }
    
    @IBAction func nextBtnTapped(_ sender: Any) {
        self.setQuestion()
    }
}
