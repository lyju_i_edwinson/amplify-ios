//
//  UserProfileTableViewController.swift
//  QuizQpp
//
//  Created by Lyju Edwinson on 30/4/19.
//  Copyright © 2019 Lyju Edwinson. All rights reserved.
//

import UIKit

class UserProfileTableViewController: UITableViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var occupationTextField: UITextField!
    
    var imagePicker = UIImagePickerController()
    
    fileprivate let s3Service: S3Service = S3Service()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileTableViewController.imageViewTapped(gesture:)))
        profileImageView.addGestureRecognizer(tapGesture)
        profileImageView.isUserInteractionEnabled = true
        imagePicker.delegate = self
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        s3Service.downloadData { (task, url, data, error) in
            DispatchQueue.main.async(execute: {
                print("Download completed")
                if let error = error {
                    print("Error downloading file \(error)")
                } else {
                    self.profileImageView.image = UIImage(data:data!)
                }
            }
            )}
    }
    
    @objc func imageViewTapped(gesture: UIGestureRecognizer) {
        //gesture.view as? UIImageView
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Cancelled picking image")
        picker.dismiss(animated: true) {
            //nothing to do
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        profileImageView.contentMode = .scaleAspectFit
        profileImageView.image = chosenImage
        picker.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func dismissBtnTapped(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func saveBtnTapped(_ sender: Any) {
        s3Service.uploadData(data: (profileImageView.image?.pngData()!)!)
    }
}
