//
//  ViewController.swift
//  QuizQpp
//
//  Created by Lyju Edwinson on 17/3/19.
//  Copyright © 2019 Lyju Edwinson. All rights reserved.
//

import UIKit
import AWSMobileClient

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userSignIn()
        
        //create question
//        let question = Question(id: nil, question: "This is new Question?")
//        var answers: [Answer] = []
//        answers.append(Answer(answer: "A", right: false))
//        answers.append(Answer(answer: "B ", right: true))
//        answers.append(Answer(answer: "C", right: false))

        //QuizService().createQuiz(question: question, answers:  answers)
        
        //let answer = Answer(id: nil, answer: "2", right: true, questionId: "59e2f471-7e54-4bfa-b935-406008cbb442")
        //QuizService().createAnswer(input: answer)
        
        //QuizService().readFromExisitngTable()
        Serverless().doInvokeAPI()
    }
    
    @IBAction func signOutBtnTapped(_ sender: Any) {
        AWSMobileClient.sharedInstance().signOut()
        self.userSignIn()
    }
    
    func userSignIn() {
        AWSMobileClient.sharedInstance().initialize { (userState, error) in
            if let userState = userState {
                switch(userState){
                case .signedIn:
                    DispatchQueue.main.async {
                        print("User already Signed In")
                    }
                case .signedOut:
                    AWSMobileClient.sharedInstance().showSignIn(navigationController: self.navigationController!, { (userState, error) in
                        if(error == nil){       //Successful signin
                            DispatchQueue.main.async {
                               print("User Signed In now")
                            }
                        }
                    })
                default:
                    AWSMobileClient.sharedInstance().signOut()
                }
                
            } else if let error = error {
                print(error.localizedDescription)
            }
        }
    }
}

