//
//  Serverless.swift
//  QuizQpp
//
//  Created by Lyju Edwinson on 3/7/19.
//  Copyright © 2019 Lyju Edwinson. All rights reserved.
//

import Foundation
import AWSAPIGateway
import AWSMobileClient

class Serverless {
    
    func doInvokeAPI() {
        // change the method name, or path or the query string parameters here as desired
        let httpMethodName = "POST"
        // change to any valid path you configured in the API
        let URLString = "/quiztest/proxy"
        let queryStringParameters = ["name":"Lyju"]
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        let httpBody = "{ \"Test From\":\"Mobile app\"}"
        
        // Construct the request object
        let apiRequest = AWSAPIGatewayRequest(httpMethod: httpMethodName,
                                              urlString: URLString,
                                              queryParameters: queryStringParameters,
                                              headerParameters: headerParameters,
                                              httpBody: httpBody)
        
        // Create a service configuration
        let serviceConfiguration = AWSServiceConfiguration(region: AWSRegionType.APSoutheast2,
                                                           credentialsProvider: AWSMobileClient.sharedInstance())
        
        // Initialize the API client using the service configuration
        QUIZLAMDAARESOURCEQuizqppfecClient.registerClient(withConfiguration: serviceConfiguration!, forKey: "quizlamdaareLambda")
        
        // Fetch the Cloud Logic client to be used for invocation
        let invocationClient = QUIZLAMDAARESOURCEQuizqppfecClient.client(forKey: "quizlamdaareLambda")
        
        invocationClient.invoke(apiRequest).continueWith { (task: AWSTask) -> Any? in
            if let error = task.error {
                print("Error occurred: \(error)")
                // Handle error here
                return nil
            }
            
            // Handle successful result here
            let result = task.result!
            let responseString = String(data: result.responseData!, encoding: .utf8)
            
            print(responseString)
            print(result.statusCode)
            
            return nil
        }
    }
}
