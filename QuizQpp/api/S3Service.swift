//
//  S3Service.swift
//  QuizQpp
//
//  Created by Lyju Edwinson on 2/5/19.
//  Copyright © 2019 Lyju Edwinson. All rights reserved.
//

import AWSS3

class S3Service {
    
    fileprivate let BUCKETNAME = "quizappuserprofileimages"
    
    
    func downloadData(completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?) {
        
        UserAPI.getUserId { (userCognitoId) in
            let transferUtility = AWSS3TransferUtility.default()
            transferUtility.downloadData(
                fromBucket: self.BUCKETNAME,
                key: "protected/\(userCognitoId)/userimage.png",
                expression: AWSS3TransferUtilityDownloadExpression(),
                completionHandler: completionHandler
                )
        }
    }
    
    func uploadData(data: Data) {
        
        let expression = AWSS3TransferUtilityUploadExpression()
        
        var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let _ = error {
                    print("There was an error uploading files... \(error.debugDescription)")
                    print("Let us check response headers \(task.response?.allHeaderFields ?? [:]) ")
                    //print("Let us check response \(task.response?.debugDescription) ")
                } else {
                    print("Completed uploading file...")
                }
            })
        }
        
        UserAPI.getUserId { (userCognitoId) in
            let transferUtility = AWSS3TransferUtility.default()
            transferUtility.uploadData(data,
                                       bucket: self.BUCKETNAME,
                                       key: "protected/\(userCognitoId)/userimage.png",
                contentType: "image/jpeg",
                expression: expression,
                completionHandler: completionHandler)
        }
    }
    
}
