//
//  QuizService.swift
//  QuizQpp
//
//  Created by Lyju Edwinson on 20/3/19.
//  Copyright © 2019 Lyju Edwinson. All rights reserved.
//

import Foundation
import AWSAppSync

class QuizService {
    
    var appSyncClient: AWSAppSyncClient?
    
    init() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appSyncClient = appDelegate.appSyncClient
    }
    
    func createQuiz(question: Question, answers: [Answer]) {
        createQuestion(input: question) { (questionId) in
            if let _ = questionId {
                for answer in answers {
                    answer.questionId = questionId!
                    self.createAnswer(input: answer)
                }
            }
        }
    }
    

    //create Question
    func createQuestion(input: Question, completion: @escaping(_ questionId: GraphQLID?) -> Void) {
        let mutationInput = CreateQuizQuestionInput(question: input.question)
        appSyncClient?.perform(mutation: CreateQuizQuestionMutation(input: mutationInput)) { (result, error) in
            if (self.hasError(errors: result?.errors, error: error)) {
                completion(nil)
            }
            
            if let id = result?.data?.createQuizQuestion?.id {
                print("Question created with id \(id)")
                completion(id)
            }
        }
    }
    
    //create answer
    func createAnswer(input: Answer) {
        let createAnswerInput = CreateQuizAnswerInput(answer: input.answer, right: input.right, question: input.questionId!)
        appSyncClient?.perform(mutation: CreateQuizAnswerMutation(input: createAnswerInput)) { (result, error) in
            
            if (self.hasError(errors: result?.errors, error: error)) {
                return
            }
            
            if let id = result?.data?.createQuizAnswer?.id {
                print("Answer crerated created with id \(id)")
            }
        }
    }
    
    func hasError(errors: [GraphQLError]?, error: Error?) -> Bool {
        if let error = error as? AWSAppSyncClientError {
            print("Error occurred: \(error.localizedDescription )")
            return true
        }
        if let resultError = errors {
            print("Error saving the item on server: \(resultError)")
            return true
        }
        return false
    }
    
    //get Questions
    func fetchQuestions(afterFetchingQuestions: @escaping ([Question]) -> Void ){
        var questions: [Question] = []
        appSyncClient?.fetch(query: ListQuizQuestionsQuery(), cachePolicy: .fetchIgnoringCacheData)  { (result, error) in
            if (!self.hasError(errors: result?.errors, error: error)) {
                result?.data?.listQuizQuestions?.items!.forEach {
                    print(($0?.question)!)
                    questions.append(Question(id: $0?.id, question: $0!.question))
                }
            }
            afterFetchingQuestions(questions)
        }
    }
    
    //get Answers
    func fetchAnswersFor(questionId: GraphQLID, afterFetchingAQnswers: @escaping ([Answer]) -> Void) {
        var answers: [Answer] = []
        let questionIdFilter = ModelIDFilterInput(eq: questionId)
        let filter = ModelQuizAnswerFilterInput(question: questionIdFilter)
        appSyncClient?.fetch(query: ListQuizAnswersQuery(filter: filter), cachePolicy: .fetchIgnoringCacheData)  { (result, error) in
            if (!self.hasError(errors: result?.errors, error: error)) {
                result?.data?.listQuizAnswers?.items!.forEach {
                    print(($0?.answer)!)
                    answers.append(Answer(id: $0!.id, answer: $0!.answer!, right: $0!.right!, questionId: $0!.question))
                }
            }
            afterFetchingAQnswers(answers)
        }
    }
    
    func createExisting() {
        let mutationInput = CreateTodelete1Input(userId: "abc", dept: "hope")
        appSyncClient?.perform(mutation: CreateTodelete1Mutation(input: mutationInput)) { (result, error) in
            if (self.hasError(errors: result?.errors, error: error)) {
            }
            
            if let id = result?.data?.createTodelete1?.userId {
                print("Another created with id \(id)")
            }
        }
    }
    
    
    func readFromExisitngTable() {
        appSyncClient?.fetch(query: ListThisIsExistingTablesQuery(), cachePolicy: .fetchIgnoringCacheData)  { (result, error) in
            if (!self.hasError(errors: result?.errors, error: error)) {
                result?.data?.listThisIsExistingTables?.items!.forEach {
                    print(($0?.userId)!)
                     print(($0?.itemId)!)
                }
            }
        }
    }
}
