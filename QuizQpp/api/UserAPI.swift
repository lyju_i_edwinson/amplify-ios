//
//  UserAPI.swift
//  QuizQpp
//
//  Created by Lyju Edwinson on 5/6/19.
//  Copyright © 2019 Lyju Edwinson. All rights reserved.
//

import AWSMobileClient

class UserAPI {
    
    static func getUserId(completion: @escaping (String) -> Void) {
        AWSMobileClient.sharedInstance().getIdentityId().continueWith { task in
            print("IdentityId received \(task.result!)")
            completion(task.result! as String)
            return nil
        }
    }
}
