//
//  Answer.swift
//  QuizQpp
//
//  Created by Lyju Edwinson on 20/3/19.
//  Copyright © 2019 Lyju Edwinson. All rights reserved.
//

import Foundation
import AWSAppSync

class Answer {
    
    var id: GraphQLID?
    var answer: String
    var right: Bool
    var questionId: GraphQLID?
    
    init(id: GraphQLID?, answer: String, right: Bool, questionId: GraphQLID?) {
        self.id = id
        self.answer = answer
        self.right = right
        self.questionId = questionId
    }
    
    convenience init(answer: String, right: Bool) {
        self.init(id: nil, answer: answer, right: right, questionId: nil)
    }
}
