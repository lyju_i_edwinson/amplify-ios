//
//  Question.swift
//  QuizQpp
//
//  Created by Lyju Edwinson on 20/3/19.
//  Copyright © 2019 Lyju Edwinson. All rights reserved.
//

import Foundation
import AWSAppSync

class Question {
    
    var id: GraphQLID?
    var question: String
    
    init(id: GraphQLID?, question: String) {
        self.id = id
        self.question = question
    }
}
